class SRSS
{
 public:
  int getsrss(double jd, double timezone,
              double lat, double lon,
              double &sr, double &ss,
              double zenith);
 private:

  double SS_, SR_;
  double B5, L5, H, Z0;
  double T, TT, T0;
  double S;
  double L,G,V,U,W;
  double A5, D5, R5;

  double Z1, C, Z, A0, D0, DA, DD, P;

  double A2, D2, V0, V2;

  int M8, W8, C0;

  double A[3], D[3];

  static const double P2;
  static const double DR;
  static const double K1;

  void init_();
  int sign(double f);
  void sub490();
  void get_sun_pos();
  void lst_zero();
};

int get_sunrise(double jd, double timezone,
                double lat, double lon,
                double &sr, double &ss,
                double zenith);
