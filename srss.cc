#include "srss.h"
#include <math.h>

int SRSS::getsrss(double jd, double timezone,
                    double lat, double lon,
                    double &sr, double &ss,
                    double zenith)
  {
    init_();

    B5 = lat; L5 = lon;
    H = timezone;
    L5 /= 360.0;
    Z0 = H/24;

    T=(jd-2451545);
    TT=T/36525+1;

    lst_zero();

    T += Z0;

    get_sun_pos();
    A[1] = A5; D[1] = D5;
    T += 1.0;
    get_sun_pos();
    A[2] = A5; D[2] = D5;

    if (A[2] < A[1])
        A[2] = A[2] + P2;

    Z1=DR*zenith;
    //Z1=DR*90.833;
    //Z1=DR*90.000;

    S=sin(B5*DR);
    C=cos(B5*DR);
    Z=cos(Z1);
    M8=0;
    W8=0;
    //PRINT
    A0=A[1];
    D0=D[1];
    DA=A[2]-A[1];
    DD=D[2]-D[1];

    for(C0=0; C0 <= 23; C0++)
    {
        P=(C0+1)/24.0;
        A2=A[1]+P*DA; D2=D[1]+P*DD;
        // 240 GOSUB 490
        sub490();
        A0=A2;
        D0=D2;
        V0=V2;
    }
    sr = SR_;
    ss = SS_;
    return 0;
  }

  void SRSS::init_()
  {
    SS_ = 0; SR_ = 0;
    W8 = 0; M8 = 0;
  }

  int SRSS::sign(double f)
  {
    if (f > 0)
      return 1;
    if (f < 0)
      return -1;
    return 0;
  }

  void SRSS::sub490()
  {
    //  Test an hour for an event
    double L0=T0+C0*K1;
    double L2=L0+K1;
    double H0=L0-A0;
    double H2=L2-A2;
    double H1=(H2+H0)/2; // : '  Hour angle,
    double D1=(D2+D0)/2; // '  declination,
    // 540 '                at half hour
    if (C0<=0)
        V0=S*sin(D0)+C*cos(D0)*cos(H0)-Z;
    V2=S*sin(D2)+C*cos(D2)*cos(H2)-Z;
    if (sign(V0) == sign(V2))
        return;
    double V1=S*sin(D1)+C*cos(D1)*cos(H1)-Z;
    double A_=2*V2-4*V1+2*V0;
    double B=4*V1-3*V0-V2;
    double D_=B*B-4*A_*V0;
    if (D_<0)
        return;
    D_=sqrt(D_);

    double E=(-B+D_)/(2*A_);
    if (E>1 || E<0)
        E=(-B-D_)/(2*A_);
    double T3=C0+E+1.0/120.0; //  Round off
    int H3=(int)floor(T3);
    int M3=(int)floor((T3-H3)*60.0);
    if (V0<0 && V2>0)
    {
        //printf("Sunrise at ");
        SR_ = H3 + M3/60.0;
        M8 = 1;
    }
    if (V0 > 0 && V2 < 0)
    {
        //    printf("Sunset at ");
        W8 = 1;
        SS_ = H3 + M3/60.0;
    }
    //printf("%02d:%02d", H3, M3);
    /*
      double H7=H0+E*(H2-H0);
      double N7=-cos(D1)*sin(H7);
      double D7=C*sin(D1)-S*cos(D1)*cos(H7);
      double AZ=atan(N7/D7)/DR;
      if (D7 < 0)
      AZ += 180;
      if (AZ < 0)
      AZ += 360;
      if (AZ > 360)
      AZ += 360;

      printf(",   azimuth %4.1f\n", AZ);
    */
  };

  void SRSS::get_sun_pos()
  {
    L=.779072+.00273790931*T;
    G=.993126+.0027377785*T;
    L=L-floor(L);
    G=G-floor(G);
    L=L*P2; G=G*P2;
    V=.39785*sin(L);
    V=V -.01000*sin(L-G);
    V=V +.00333*sin(L+G);
    V=V -.00021*TT*sin(L);
    U=1-.03349*cos(G);
    U=U-.00014*cos(2*L);
    U=U+.00008*cos(L);
    W=-.00010-.04129*sin(2*L);
    W=W+.03211*sin(G);
    W=W+.00104*sin(2*L-G);
    W=W-.00035*sin(2*L+G);
    W=W-.00008*TT*sin(G);
    S=W/sqrt(U-V*V);
    A5=L+atan(S/sqrt(1-S*S));
    S=V/sqrt(U);
    D5=atan(S/sqrt(1-S*S));
    R5=1.00021*sqrt(U);
  }

  void SRSS::lst_zero()
  {
    T0=T/36525;
    S=24110.5+8640184.813*T0;
    S=S+86636.6*Z0+86400*L5;
    S=S/86400;
    S=S-floor(S);
    T0=S*360*DR;
  }

  const double SRSS::P2 = M_PI*2.0;
  const double SRSS::DR = M_PI/180.0;
  const double SRSS::K1 = 15*(M_PI/180.0)*1.0027379;

  int get_sunrise(double jd, double timezone,
                  double lat, double lon,
                  double &sr, double &ss,
                  double zenith) {
    SRSS s;
    return s.getsrss(jd, timezone, lat, lon, sr, ss, zenith);
  }
